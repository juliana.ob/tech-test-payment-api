﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Payments.Application.Mapping;
using Payments.Application.Services;
using Payments.Application.Services.Interfaces;
using Payments.Domain.Repositories;
using Payments.Infra.Data.Context;
using Payments.Infra.Data.Repositories;

namespace Payments.Infra.IoC
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options => options.UseInMemoryDatabase(databaseName: "PaymentsDb"));
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ISalesmanRepository, SalesmanRepository>();
            services.AddScoped<ISalesOrderRepository, SalesOrderRepository>();

            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(DomainToInputMapping));
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ISalesmanService, SalesmanService>();
            services.AddScoped<ISalesOrderService, SalesOrderService>();
            return services;
        }
    }
}
