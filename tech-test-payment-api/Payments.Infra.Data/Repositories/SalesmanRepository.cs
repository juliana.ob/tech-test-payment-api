﻿using Microsoft.EntityFrameworkCore;
using Payments.Domain.Entities;
using Payments.Domain.Repositories;
using Payments.Infra.Data.Context;

namespace Payments.Infra.Data.Repositories
{
    public class SalesmanRepository : ISalesmanRepository
    {
        private readonly DataContext _dbContext;
        public SalesmanRepository(DataContext dbContext) 
        { 
            _dbContext= dbContext;
        }

        public async Task<Salesman> AddSalesmanAsync(Salesman salesman)
        {
            _dbContext.Salesmens.Add(salesman);
            _dbContext.SaveChanges();
            return await Task.FromResult(salesman);
        }

        public async Task<List<Salesman>> GetAllAsync()
        {
            return await _dbContext.Salesmens.ToListAsync();
        }

        public async Task<Salesman> GetByIdAsync(int id)
        {
            return await _dbContext.Salesmens.FirstOrDefaultAsync(o => o.Id == id);
        }
    }
}
