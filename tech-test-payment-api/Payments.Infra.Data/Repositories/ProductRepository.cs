﻿using Microsoft.EntityFrameworkCore;
using Payments.Domain.Entities;
using Payments.Domain.Repositories;
using Payments.Infra.Data.Context;

namespace Payments.Infra.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _dbContext;
        public ProductRepository(DataContext dbContext) 
        { 
            _dbContext = dbContext;
        }

        public async Task<Product> AddProductAsync(Product product)
        {
            _dbContext.Products.Add(product);
            _dbContext.SaveChanges();
            return await Task.FromResult(product);
        }

        public async Task<List<Product>> GetAllAsync()
        {
           return await _dbContext.Products.ToListAsync();
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            return await _dbContext.Products.FirstOrDefaultAsync(o => o.Id == id);
        }
    }
}
