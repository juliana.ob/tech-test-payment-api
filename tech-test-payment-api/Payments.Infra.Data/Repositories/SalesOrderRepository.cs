﻿using Microsoft.EntityFrameworkCore;
using Payments.Domain.Entities;
using Payments.Domain.Repositories;
using Payments.Infra.Data.Context;

namespace Payments.Infra.Data.Repositories
{
    public class SalesOrderRepository : ISalesOrderRepository
    {
        private readonly DataContext _dbContext;
        public SalesOrderRepository(DataContext dbContext)
        {
            _dbContext= dbContext;
        }
        public async Task<SalesOrder> RegisterSalesOrderAsync(SalesOrder salesorder)
        {
            _dbContext.SalesOrders.Add(salesorder);
            _dbContext.SaveChangesAsync();
            return await Task.FromResult(salesorder);
        }

        public async Task<SalesOrder> GetByIdAsync(int id)
        {
            return await _dbContext.SalesOrders.Where(o => o.Id == id).FirstOrDefaultAsync();
        }
        
        public async Task<SalesOrder> EditStatusByIdAsync(SalesOrder salesorder)
        {
            _dbContext.SalesOrders.Update(salesorder);
            _dbContext.SaveChangesAsync();
            return await Task.FromResult(salesorder);
        }
    }
}
