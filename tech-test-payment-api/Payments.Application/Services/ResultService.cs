﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payments.Application.Services
{
    public class ResultService
    {
        public bool IsSuccess { get; set; }
        public ICollection<Errors>? Errors { get; set; }
        public string? Message { get; set; }

        public static ResultService Invalid(string message) => new ResultService{IsSuccess = false, Message = message};
        public static ResultService<T> Invalid<T>(string message) => new ResultService<T> {IsSuccess = false, Message = message};

        public static ResultService Ok(string message) => new ResultService { IsSuccess = true, Message = message };
        public static ResultService<T> Ok<T>(T data) => new ResultService<T> { IsSuccess = true, Data = data };
    }

    public class ResultService<T> : ResultService
    {
        public T Data { get; set; }
    }
}
