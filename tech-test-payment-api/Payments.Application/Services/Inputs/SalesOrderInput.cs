﻿using Payments.Domain.Entities;
using Payments.Domain.Shared.Enums;

namespace Payments.Application.Services.Inputs
{
    public  class SalesOrderInput 
    {
        public int Id { get; set; }
        public int SalesmanId { get; set; }
        public List<int> Products { get; set; }
        public DateTime SalesDate { get; set; }
        public SalesStatus status { get; set; }

        public static SalesOrder SalesOrderConvert(SalesOrderInput input, List<Product> products) => new SalesOrder
        {
            Id = input.Id,
            SalesmanId = input.SalesmanId,
            SalesDate= input.SalesDate,
            status = SalesStatus.AwaitingPayment,
            Products = products            
        }; 
        
        public static SalesOrder SalesOrderConvertEdit(SalesOrderInput input, SalesOrder salesOrder) => new SalesOrder
        {
            SalesmanId = input.SalesmanId,
            SalesDate= input.SalesDate,
            status = input.status, 
            Products = salesOrder.Products
        };        
    }
}
