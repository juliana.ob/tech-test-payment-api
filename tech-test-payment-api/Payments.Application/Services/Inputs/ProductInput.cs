﻿namespace Payments.Application.Services.Inputs
{
    public class ProductInput
    {
        public int Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
    }
}
