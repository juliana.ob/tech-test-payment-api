﻿using AutoMapper;
using Payments.Application.Services.Inputs;
using Payments.Application.Services.Interfaces;
using Payments.Domain.Entities;
using Payments.Domain.Repositories;

namespace Payments.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper= mapper;    
        }

        public async Task<ResultService<ProductInput>> CreateProductAsync(ProductInput productInput)
        {
            if (productInput is null)
                return ResultService.Invalid<ProductInput>("Input is null") ;

            var productMapper = _mapper.Map<Product>(productInput);
            var result = await _productRepository.AddProductAsync(productMapper);

            return ResultService.Ok<ProductInput>(_mapper.Map<ProductInput>(result));
        }
        
        public async Task<ResultService<List<ProductInput>>> GetAllAsync()
        {
            var result = await _productRepository.GetAllAsync();

            return ResultService.Ok<List<ProductInput>>(_mapper.Map<List<ProductInput>>(result));
        }

        public async Task<ResultService<ProductInput>> GetByIdAsync(int id)
        {
            var result = await _productRepository.GetByIdAsync(id);
            if (result is null)
                return ResultService.Invalid<ProductInput>("Produto não encontrato!");

            return ResultService.Ok<ProductInput>(_mapper.Map<ProductInput>(result));
        }

    }
}
