﻿using Payments.Application.Services.Inputs;

namespace Payments.Application.Services.Interfaces
{
    public interface ISalesOrderService
    {
        Task<ResultService> RegisterSalesOrderAsync(SalesOrderInput salesOrderInput);
        Task<ResultService<SalesOrderInput>> GetByIdAsync(int id);
        Task<ResultService> EditStatusByIdAsync(SalesOrderInput salesOrderInput);
    }
}
