﻿using Payments.Application.Services.Inputs;

namespace Payments.Application.Services.Interfaces
{
    public interface IProductService 
    {
        Task<ResultService<ProductInput>> CreateProductAsync(ProductInput productInput);
        Task<ResultService<List<ProductInput>>> GetAllAsync();
        Task<ResultService<ProductInput>> GetByIdAsync(int id);
    }
}
