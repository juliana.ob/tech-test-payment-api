﻿using Payments.Application.Services.Inputs;

namespace Payments.Application.Services.Interfaces
{
    public interface ISalesmanService
    {
        Task<ResultService<SalesmanInput>> CreateSalesmanAsync(SalesmanInput salesmanInput);
        Task<ResultService<List<SalesmanInput>>> GetAllAsync();
        Task<ResultService<SalesmanInput>> GetByIdAsync(int id);
    }
}
