﻿namespace Payments.Application.Services
{
    public class Errors
    {
        public string? Code { get; set; }
        public string? Message { get; set; }
    }
}
