﻿using AutoMapper;
using Payments.Application.Services.Inputs;
using Payments.Application.Services.Interfaces;
using Payments.Domain.Entities;
using Payments.Domain.Repositories;
using Payments.Domain.Shared.Enums;

namespace Payments.Application.Services
{
    public class SalesOrderService : ISalesOrderService
    {
        private readonly ISalesOrderRepository _salesOrderRepository;
        private readonly IProductRepository _productRepository;
        private readonly ISalesmanRepository _salesmanRepository;
        private readonly IMapper _mapper;
        public SalesOrderService(ISalesOrderRepository salesOrderRepository, 
                                 IMapper mapper,
                                 IProductRepository productRepository,
                                 ISalesmanRepository salesmanRepository)
        {            
            _mapper= mapper;  
            _salesOrderRepository= salesOrderRepository;
            _productRepository= productRepository;
            _salesmanRepository= salesmanRepository;
        }

        public async Task<ResultService> RegisterSalesOrderAsync(SalesOrderInput salesOrderInput)
        {
            if (salesOrderInput is null)
                return ResultService.Invalid<SalesOrderInput>("Inserir os dados corretamente!") ;

            var listProducts = GetListProducts(salesOrderInput.Products); 

            var salesOrderConvert = SalesOrderInput.SalesOrderConvert(salesOrderInput, listProducts);

            var salesOrderMapper = _mapper.Map<SalesOrder>(salesOrderConvert);            

            if (salesOrderMapper.Products.Count() < 1)
                return ResultService.Invalid<SalesOrderInput>("Adicione pelo menos um produto no carrinho!");                  

            var result = await _salesOrderRepository.RegisterSalesOrderAsync(salesOrderMapper);

            return ResultService.Ok("Pedido gravado com sucesso!");
        }        
       

        public async Task<ResultService<SalesOrderInput>> GetByIdAsync(int id)
        {
            var result = await _salesOrderRepository.GetByIdAsync(id);

            if (result is null)
                return ResultService.Invalid<SalesOrderInput>("Pedido não encontrato!");

            return ResultService.Ok(_mapper.Map<SalesOrderInput>(result));
        }

        public async Task<ResultService> EditStatusByIdAsync(SalesOrderInput salesOrderInput)
        {            
            if(salesOrderInput is null)
                return ResultService.Invalid<SalesOrderInput>("Objeto deve ser informado!");

            var salesOrder = await _salesOrderRepository.GetByIdAsync(salesOrderInput.Id);
            if (salesOrder is null)
                return ResultService.Invalid<SalesOrderInput>("Pedido não encontrato!");

            var statusInput = GetRightStatus(salesOrderInput.status, salesOrder.status);
            if (statusInput != "")
                return ResultService.Invalid<SalesOrderInput>(statusInput);

            salesOrder = SalesOrderInput.SalesOrderConvertEdit(salesOrderInput, salesOrder);

            await _salesOrderRepository.EditStatusByIdAsync(salesOrder);

            return ResultService.Ok("Editado com sucesso!");
        }

        public List<Product> GetListProducts(List<int> listProductIds)
        {            
            var productList = _productRepository.GetAllAsync();
            productList.Result.Where(c => listProductIds.Contains(c.Id)).ToList();            
            
            return productList.Result;
        }

        private string GetRightStatus(SalesStatus statusInput, SalesStatus statusOrigem)
        {
            switch (statusOrigem)
            {
                case SalesStatus.AwaitingPayment:
                    if (statusInput != SalesStatus.Approved && statusInput != SalesStatus.Canceled)
                    {
                        return "Status Inválido! Insira um status válido.";
                    }
                    break;
                case SalesStatus.Approved:
                    if (statusInput != SalesStatus.SentToCarried && statusInput != SalesStatus.Canceled)
                    {
                        return "Status Inválido! Insira um status válido.";
                    }
                    break;
                case SalesStatus.SentToCarried:
                    if (statusInput != SalesStatus.Delivered)
                    {
                        return "Status Inválido! Insira um status válido.";
                    }
                    break;
            }
            return "";
        }
    }
}
