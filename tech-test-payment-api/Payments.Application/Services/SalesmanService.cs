﻿using AutoMapper;
using Payments.Application.Services.Inputs;
using Payments.Application.Services.Interfaces;
using Payments.Domain.Entities;
using Payments.Domain.Repositories;

namespace Payments.Application.Services
{
    public class SalesmanService : ISalesmanService
    {
        private readonly ISalesmanRepository _salesmanRepository;
        private readonly IMapper _mapper;
        public SalesmanService(ISalesmanRepository salesmanRepository, IMapper mapper)
        {
            _salesmanRepository = salesmanRepository;
            _mapper= mapper;    
        }

        public async Task<ResultService<SalesmanInput>> CreateSalesmanAsync(SalesmanInput SalesmanInput)
        {
            if (SalesmanInput is null)
                return ResultService.Invalid<SalesmanInput>("Input is null") ;

            var SalesmanMapper = _mapper.Map<Salesman>(SalesmanInput);
            var result = await _salesmanRepository.AddSalesmanAsync(SalesmanMapper);

            return ResultService.Ok<SalesmanInput>(_mapper.Map<SalesmanInput>(result));
        }
        
        public async Task<ResultService<List<SalesmanInput>>> GetAllAsync()
        {
            var result = await _salesmanRepository.GetAllAsync();

            return ResultService.Ok<List<SalesmanInput>>(_mapper.Map<List<SalesmanInput>>(result));
        }

        public async Task<ResultService<SalesmanInput>> GetByIdAsync(int id)
        {
            var result = await _salesmanRepository.GetByIdAsync(id);
            if (result is null)
                return ResultService.Invalid<SalesmanInput>("Vendedor não encontrato!");

            return ResultService.Ok<SalesmanInput>(_mapper.Map<SalesmanInput>(result));
        }

    }
}
