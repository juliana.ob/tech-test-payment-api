﻿using AutoMapper;
using Payments.Application.Services.Inputs;
using Payments.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payments.Application.Mapping
{
    public class InputToDomainMapping : Profile
    {
        public InputToDomainMapping()
        {
            CreateMap<ProductInput, Product>();
            CreateMap<SalesmanInput,Salesman>();
            CreateMap<SalesOrderInput, SalesOrder>();
        }
    }
}
