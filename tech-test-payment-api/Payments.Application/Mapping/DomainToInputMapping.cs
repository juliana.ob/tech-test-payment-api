﻿using AutoMapper;
using Payments.Application.Services.Inputs;
using Payments.Domain.Entities;

namespace Payments.Application.Mapping
{
    public class DomainToInputMapping : Profile
    {
        public DomainToInputMapping()
        {
            CreateMap<Product, ProductInput>();
            CreateMap<Salesman, SalesmanInput>();
            CreateMap<SalesOrder, SalesOrderInput>();
        }
    }
}
