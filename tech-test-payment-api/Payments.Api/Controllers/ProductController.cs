﻿using Microsoft.AspNetCore.Mvc;
using Payments.Application.Services.Inputs;
using Payments.Application.Services.Interfaces;

namespace Payments.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService= productService;
        }

        [HttpPost]        
        public async Task<ActionResult<ProductInput>> PostProduct([FromBody] ProductInput input)
        {
            var result = await _productService.CreateProductAsync(input);
            if(!result.IsSuccess) 
                return BadRequest(result);

            return Created("api/[controller]", result);
        } 
        
        [HttpGet]        
        public async Task<ActionResult<ProductInput>> GetAllProduct()
        {
            var result = await _productService.GetAllAsync();
            return Ok(result);
        }
        
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<ProductInput>> GetByIdProduct(int id)
        {
            var result = await _productService.GetByIdAsync(id);
            if(!result.IsSuccess)
                return NotFound(result);
            return Ok(result);
        }

    }
}
