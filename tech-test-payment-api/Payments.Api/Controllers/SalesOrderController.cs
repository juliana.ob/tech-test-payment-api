﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Payments.Application.Services.Inputs;
using Payments.Application.Services.Interfaces;

namespace Payments.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesOrderController : ControllerBase
    {
        private readonly ISalesOrderService _salesOrderService;
        public SalesOrderController(ISalesOrderService salesOrderService)
        {
            _salesOrderService = salesOrderService;
        }

        [HttpPost]
        public async Task<ActionResult<SalesOrderInput>> PostSalesOrder([FromBody] SalesOrderInput input)
        {
            var result = await _salesOrderService.RegisterSalesOrderAsync(input);
            if (!result.IsSuccess)
                return BadRequest(result);

            return Created("api/[controller]", result);
        }
        

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<SalesOrderInput>> GetSalesOrderById(int id)
        {
            var result = await _salesOrderService.GetByIdAsync(id);
            if (!result.IsSuccess)
                return NotFound(result);
            return Ok(result);
        }
        
        [HttpPut]
        public async Task<ActionResult<SalesOrderInput>> EditStatusById([FromBody] SalesOrderInput input)
        {
            var result = await _salesOrderService.EditStatusByIdAsync(input);
            return Ok(result);
        }
    }
}
