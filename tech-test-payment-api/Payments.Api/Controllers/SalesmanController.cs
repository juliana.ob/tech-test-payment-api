﻿using Microsoft.AspNetCore.Mvc;
using Payments.Application.Services.Inputs;
using Payments.Application.Services.Interfaces;

namespace Payments.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesmanController : ControllerBase
    {
        private readonly ISalesmanService _salesmanService;
        public SalesmanController(ISalesmanService salesmanService)
        {
            _salesmanService = salesmanService;
        }

        [HttpPost]
        public async Task<ActionResult<SalesmanInput>> PostProduct([FromBody] SalesmanInput input)
        {
            var result = await _salesmanService.CreateSalesmanAsync(input);
            if (!result.IsSuccess)
                return BadRequest(result);

            return Created("api/[controller]", result);
        }

        [HttpGet]
        public async Task<ActionResult<SalesmanInput>> GetAllProduct()
        {
            var result = await _salesmanService.GetAllAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<SalesmanInput>> GetByIdSalesman(int id)
        {
            var result = await _salesmanService.GetByIdAsync(id);
            if (!result.IsSuccess)
                return NotFound(result);
            return Ok(result);
        }
    }
}
