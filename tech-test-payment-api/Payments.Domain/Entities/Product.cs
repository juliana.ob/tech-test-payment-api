﻿namespace Payments.Domain.Entities
{
    public class Product 
    {
        public int Id { get; set; }
        public string Code { get;  set; }
        public string Name { get;  set; }
        public SalesOrder SalesOrders { get; set; }
    }
}
