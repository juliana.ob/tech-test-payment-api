﻿using Payments.Domain.Shared.Enums;

namespace Payments.Domain.Entities
{
    public class SalesOrder 
    {
        public int Id { get; set; }
        public int SalesmanId { get; set; }
        public List<Product> Products { get; set; }
        public DateTime SalesDate { get; set; }
        public SalesStatus status { get; set; }
        public Salesman Salesman { get; set; }
        
    }
}
