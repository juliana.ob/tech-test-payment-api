﻿using Payments.Domain.Entities;

namespace Payments.Domain.Repositories
{
    public interface ISalesOrderRepository
    {
        Task<SalesOrder> RegisterSalesOrderAsync(SalesOrder salesorder);
        Task<SalesOrder> GetByIdAsync(int id);
        Task<SalesOrder> EditStatusByIdAsync(SalesOrder salesorder);
    }
}
