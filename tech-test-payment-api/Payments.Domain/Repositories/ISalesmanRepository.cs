﻿using Payments.Domain.Entities;

namespace Payments.Domain.Repositories
{
    public interface ISalesmanRepository
    {
        Task<List<Salesman>> GetAllAsync();
        Task<Salesman> GetByIdAsync(int id);
        Task<Salesman> AddSalesmanAsync(Salesman salesman);
    }
}
