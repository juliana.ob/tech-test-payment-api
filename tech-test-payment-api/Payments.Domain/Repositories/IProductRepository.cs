﻿using Payments.Domain.Entities;

namespace Payments.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task<Product> AddProductAsync(Product product);
    }
}
