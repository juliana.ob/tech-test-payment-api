﻿namespace Payments.Domain.Shared.Enums
{
    public enum SalesStatus
    {
        Approved,
        Canceled, 
        SentToCarried,
        Delivered,
        AwaitingPayment
    }
}
